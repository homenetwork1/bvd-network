# Ansible

## setup ansible

```bash
sudo apt install ansible -y
```

adjust ansible.cfg file

```bash
sudo vim /etc/ansible/ansible.cfg
# uncomment
log_path = /var/log/ansible.log
private_key_file = /home/<user>/.ssh/id_rsa
```
