# Network layout

project to document the layout of the home network

## Ip ranges

| range   | devices         |
| ------- | --------------- |
| 1-20    | network devices |
| 21-40   | to be defined   |
| 41-60   | storage         |
| 61-80   | person laptops  |
| 81-100  | person desktops |
| 101-120 | phones          |
| 121-140 | media/game      |
| 141-160 | applications    |
| 161-180 | swarm servers   |
| 181-200 | to be defined   |
| 200-256 | DHCP            |

## definded IP's

| IP            | device       | device type    | function                 | project                                    |
| ------------- | ------------ | -------------- | ------------------------ | ------------------------------------------ |
| 192.168.1.1   | Modem/Router | Basic Router   | configured as modem      | /                                          |
| 192.168.0.1   | Router       | tp-link Router |                          | /                                          |
| 192.168.0.2   | Switch       | tp-link Switch |                          | /                                          |
| 192.168.0.3   | Pihole       | Pi3            | Network-wide Ad Blocking | https://gitlab.com/homenetwork1/bvd-pihole |
| 192.168.0.41  | FreeNas      | Server         | NAS                      | /                                          |
| 192.168.0.81  | Home main pc | PC             | main computer            | /                                          |
| 192.168.0.121 | Kodi         | Pi4 4GB        | media center             | /                                          |
| 192.168.0.141 | Transmission | Server         | torrent downloader       | /                                          |
| 192.168.0.142 | Octoprint    | Pi3            | 3d printing gui          | https://gitlab.com/bvdprinter/octoprinter  |
| 192.168.0.161 | Swarm-L010   | Pi4 4GB        | swarm leader             |                                            |
| 192.168.0.162 | Swarm-W020   | Pi4 4GB        | swarm worker             |                                            |
| 192.168.0.163 | Swarm-W030   | Pi4 4GB        | swarm worker             |                                            |
| 192.168.0.164 | Swarm-W040   | Pi3            | swarm worker             |                                            |
