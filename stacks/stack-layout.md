# swarm stack layout

| IP            | device       | device type    | labels                   |
| ------------- | ------------ | -------------- | ------------------------ |
| 192.168.0.161 | Swarm-L010   | Pi4 4GB        | device=PI4, RAM=4GB      |
| 192.168.0.162 | Swarm-W020   | Pi4 4GB        | device=PI4, RAM=4GB, NFS |
| 192.168.0.163 | Swarm-W030   | Pi4 4GB        | device=PI4, RAM=4GB      |
| 192.168.0.164 | Swarm-W040   | Pi3            | device=PI3, RAM=1GB      |

